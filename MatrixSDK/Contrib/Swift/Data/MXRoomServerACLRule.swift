/*
 * Copyright (c) 2023 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import Foundation

// bwi: #4846 add federation rule on room creation
/**
 Room server acl rule.
 */
public struct MXRoomServerACLRule {
    
    /** 
     The server names to allow in the room, excluding any port information. Each entry is interpreted as a glob-style pattern.
     This defaults to an empty list when not provided, effectively disallowing every server.
     */
    var allow: [String]?
    
    /**
     The server names to disallow in the room, excluding any port information. Each entry is interpreted as a glob-style pattern.
     This defaults to an empty list when not provided.
     */
    var deny: [String]?
    
    
    /**
     True to allow server names that are IP address literals. False to deny. Defaults to true if missing or otherwise not a boolean.
     This is strongly recommended to be set to false as servers running with IP literal names are strongly discouraged in order to require legitimate homeservers to be backed by a valid registered domain name.
     */
    var allowIPLiterals: Bool
    
    
    public init?(allow: [String]? = nil, deny: [String]? = nil, allowIPLiterals: Bool = false) {
        self.allow = allow
        self.deny = deny
        self.allowIPLiterals = allowIPLiterals
    }
}
