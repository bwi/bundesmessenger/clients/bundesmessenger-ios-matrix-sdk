<div align="center">
  <img src="https://gitlab.opencode.de/bwi/bundesmessenger/info/-/raw/main/images/logo.png?inline=false" alt="BundesMessenger Logo" width="128" height="128">
</div>

<div align="center">
  <h2 align="center">BundesMessenger-iOS SDK</h2>
</div>

----

Wir freuen uns, dass Du Dich für den BundesMessenger interessierst. 

BundesMessenger-iOS SDK ist das iOS Matrix SDK basierend auf [Matrix iOS SDK](https://github.com/matrix-org/matrix-ios-sdk)
von [Element Software](https://element.io/).

Allgemeine Infos zum Thema BundesMessenger und was dahinter steckt findest Du [hier](https://gitlab.opencode.de/bwi/bundesmessenger/info) und alles weitere  zur iOS App findest Du unter [BundesMessenger iOS](https://gitlab.opencode.de/bwi/bundesmessenger/clients/bundesmessenger-ios).

## Rechtliches

Die Lizenz des BundesMessenger iOS SDK ist die [AGPLv3](./LICENSE).

### Copyright

- [BWI GmbH](https://messenger.bwi.de/copyright)
- [Element](https://element.io/copyright)

