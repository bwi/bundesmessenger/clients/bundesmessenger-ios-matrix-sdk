Changes in BWI project 2.24.0 (2025-02-14)
===================================================

Upstream merge ✨:

Features ✨:

Improvements 🙌:
- MESSENGER-6879 support of authenticated media for conntent scanner request

Bugfix 🐛:

Translations 🗣   :

SDK API changes ⚠️:

Build 🧱:
- MESSENGER-6879 use xcode 15.2 for builds

Documentation 📄:

Changes in BWI project 2.23.1 (2025-01-07)
===================================================

Upstream merge ✨:

Features ✨:

Improvements 🙌:

Bugfix 🐛:
- MESSENGER-6726 force logout in case of incorrect pin
- MESSENGER-6846 migrate files away from appgroup

Translations 🗣   :

SDK API changes ⚠️:

Build 🧱:

Documentation 📄:

Changes in BWI project 2.23.0 (2024-11-04)
===================================================

Upstream merge ✨:
- v0.27.15

Features ✨:

Improvements 🙌:

Bugfix 🐛:

Translations 🗣   :

SDK API changes ⚠️:

Build 🧱:

Documentation 📄:

Changes in BWI project 2.22.0 (2024-10-09)
===================================================

Upstream merge ✨:
- v0.27.14

Features ✨:

Improvements 🙌:

Bugfix 🐛:

Translations 🗣   :

SDK API changes ⚠️:

Build 🧱:

Documentation 📄:

Changes in BWI project 2.21.0 (2024-08-09)
===================================================

Upstream merge ✨:
- v0.27.5

Features ✨:
- MESSENGER-6245 matomo logging error status
- MESSENGER-4172 unread poll messages

Improvements 🙌:

Bugfix 🐛:
- MESSENGER-4172 unread messages from poll vote events handles more edge cases

Translations 🗣   :

SDK API changes ⚠️:

Build 🧱:

Documentation 📄:
- MESSENGER-6226 new license file

Changes in BWI project 2.17.0 (2024-04-11)
===================================================

Upstream merge ✨:
- v0.27.5

Features ✨:

Improvements 🙌:

Bugfix 🐛:
- Delete location sharing events were still visible as live locations (#5806)

Translations 🗣   :

SDK API changes ⚠️:

Build 🧱:

Changes in BWI project 2.16.0 (2024-02-13)
===================================================

Upstream merge ✨:
- v0.27.5

Features ✨:

Improvements 🙌:
- Show ACL related status events in timeline (#5575)

Bugfix 🐛:

Translations 🗣  :

SDK API changes ⚠️:

Build 🧱:

Changes in BWI project 2.15.0 (2024-02-13)
===================================================

Upstream merge ✨:
- v0.27.5

Features ✨:

Improvements 🙌:
- Refactored wellknown sync (#4478)

Bugfix 🐛:

Translations 🗣  :

SDK API changes ⚠️:

Build 🧱:

Changes in BWI project 2.14.1 (2024-01-18)
===================================================

Upstream merge ✨:

- v0.27.4

Features ✨:

Improvements 🙌:

Bugfix 🐛:
- Small fix for relabeled requests (#5448)

Translations 🗣  :

SDK API changes ⚠️:

Build 🧱:

Changes in BWI project 2.14.0 (2024-01-16)
===================================================

Upstream merge ✨:

- v0.27.4

Features ✨:

Improvements 🙌:

Bugfix 🐛:
- Relabeled request error at first login does not cause problems anymore (#5448)

Translations 🗣  :

SDK API changes ⚠️:

Build 🧱:
- Enable Renovate

Changes in BWI project 2.12.1 (2023-12-11)
===================================================

Upstream merge ✨:

- v0.27.3

Features ✨:

Improvements 🙌:
- Features ✨: Can create ACLs on room creation (#4846)

Bugfix 🐛:

Translations 🗣  :

SDK API changes ⚠️:

Build 🧱:

Changes in BWI project 2.12.0 (2023-11-20)
===================================================

Upstream merge ✨:

- v0.27.3

Features ✨:

Improvements 🙌:
- Add server acl event type for federation (#5203)

Bugfix 🐛:

Translations 🗣  :

SDK API changes ⚠️:

Build 🧱:

Changes in BWI project 2.11.0 (2023-09-28)
===================================================

Upstream merge ✨:

- v0.27.2

Features ✨:

Improvements 🙌:
- Add Rust Crypto error information (#4821)
- Disable Rust logging in Release config (#5252)

Bugfix 🐛:

Translations 🗣  :

SDK API changes ⚠️:

Build 🧱:

Changes in BWI project 2.10.0 (2023-09-26)
===================================================

Upstream merge ✨:

- v0.27.1

Features ✨:

Improvements 🙌:


Bugfix 🐛:


Translations 🗣   :

SDK API changes ⚠️:

Build 🧱:

Changes in BWI project 2.9.0 (2023-08-31)
===================================================

Upstream merge ✨:

- v0.26.12

Features ✨:
- Default powerlevels for huddle on room creating (#4928)

Improvements 🙌:


Bugfix 🐛:


Translations 🗣  :

SDK API changes ⚠️:

Build 🧱:
- New Versioning thats follows Messenger versioning

Changes in BWI project 0.26.12 (2023-07-10)
===================================================

Upstream merge ✨:

- v0.26.12

Features ✨:


Improvements 🙌:
- Migration process information (#4905)

Bugfix 🐛:


Translations 🗣  :

SDK API changes ⚠️:

Build 🧱:


Changes in BWI project 0.26.10 (2023-06-09)
===================================================

Upstream merge ✨:

- v0.26.10

Features ✨:
- Poll participant details - modell changes (#4383)

Improvements 🙌:
- Enable old encryption for prod version again (#4889)

Bugfix 🐛:


Translations 🗣  :

SDK API changes ⚠️:

Build 🧱:


=======================================================
+        TEMPLATE WHEN PREPARING A NEW RELEASE        +
=======================================================


Changes in BWI project 1.X.X (2020-XX-XX)
===================================================

Upstream merge ✨:
 -

Features ✨:
 -

Improvements 🙌:
 -

Bugfix 🐛:
 -

Translations 🗣:
 -

SDK API changes ⚠️:
 - 

Build 🧱:
 -

Other changes:
 -
